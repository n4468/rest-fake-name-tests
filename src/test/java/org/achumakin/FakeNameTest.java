package org.achumakin;

import org.achumakin.client.ApiClient;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

public class FakeNameTest {

    private static ApiClient apiClient;

    @BeforeAll
    static void setUp() {
        apiClient = new ApiClient();
    }

    @Test
    void getRandomName() {
        var response = apiClient.getName();
        assertThat(response.getFirstName()).isNotEmpty();
    }

    @Test
    void namesAreAlwaysRandom() {
        var response1 = apiClient.getName();
        var response2 = apiClient.getName();
        assertSoftly(softAssertions -> {
            softAssertions.assertThat(response1.getFirstName()).isNotEqualTo(response2.getFirstName());
            softAssertions.assertThat(response1.getLastName()).isNotEqualTo(response2.getLastName());
        });
    }

}
