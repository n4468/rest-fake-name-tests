package org.achumakin.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.extern.slf4j.Slf4j;
import org.achumakin.model.config.BaseYamlModel;

import java.io.IOException;

@Slf4j
public class YamlReader {

    private final static String CONFIG_NAME = "test-config.yaml";
    private final ObjectMapper objectMapper;

    public YamlReader() {
        objectMapper = new ObjectMapper(new YAMLFactory());
    }

    public BaseYamlModel readConfig() {
        try {
            log.info("Parsing yaml config {}", CONFIG_NAME);
            var is = this.getClass().getClassLoader().getResourceAsStream(CONFIG_NAME);
            return objectMapper.readValue(is, BaseYamlModel.class);
        } catch (IOException ex) {
            log.error("Cannot read YAML config to model {}", BaseYamlModel.class, ex);
            return null;
        }
    }

}
